package main

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
)

func main() {

	// New app
	a := app.New()
	// New Window & title
	w := a.NewWindow("Resize any widget")
	//Resize main/parent window
	w.Resize(fyne.NewSize(400, 400))
	// 1st Widget Entry
	entry := widget.NewEntry()
	entry.SetPlaceHolder("Enter your name")
	entry.Resize(fyne.NewSize(250, 30)) // my widget size
	entry.Move(fyne.NewPos(40, 50))     // position of widget

	entry_email := widget.NewEntry()
	entry_email.SetPlaceHolder("Enter your key")
	entry_email.Resize(fyne.NewSize(250, 30)) // my widget size
	entry_email.Move(fyne.NewPos(40, 100))    // position of widget

	// entry_address := widget.NewEntry()
	// entry_address.SetPlaceHolder("Enter your name")
	// entry_address.Resize(fyne.NewSize(250, 30)) // my widget size
	// entry_address.Move(fyne.NewPos(40, 150))    // position of widget

	// button
	btn_submit := widget.NewButton("Submit", nil)
	btn_submit.Resize(fyne.NewSize(150, 30)) // my widget size
	btn_submit.Move(fyne.NewPos(40, 200))    // position of widget

	w.SetContent(

		container.NewWithoutLayout(
			entry,
			entry_email,
			
			btn_submit,
		),
	)
	// a := app.New()
	// w := a.NewWindow("Consumer App")
	// w.Resize(fyne.NewSize(400, 500))

	// addBtn := widget.NewButton("Login:", func() {
	// 	username := widget.NewEntry()
	// 	username.Validator = validation.NewRegexp(`^[A-Za-z0-9_-]+$`, "username can only contain letters, numbers, '_', and '-'")
	// 	username.Resize(fyne.NewSize(400,400))
	// 	username.Move(fyne.NewPos(40,50))
	// 	password := widget.NewPasswordEntry()
	// 	password.Validator = validation.NewRegexp(`^[A-Za-z0-9_-]+$`, "password can only contain letters, numbers, '_', and '-'")
	// 	// remember := false
	// 	items := []*widget.FormItem{
	// 		widget.NewFormItem("Username", username),
	// 		widget.NewFormItem("Key", password),
	// 		// widget.NewFormItem("Remember me", widget.NewCheck("", func(checked bool) {
	// 		// 	remember = checked
	// 		// })),
	// 	}

	// 	dialog.ShowForm("Login...", "Log In", "Cancel", items, func(b bool) {
	// 		if !b {
	// 			return
	// 		}
	// 		// var rememberText string
	// 		// if remember {
	// 		// 	rememberText = "and remember this login"
	// 		// }

	// 		log.Println("Please Authenticate", username.Text, password.Text)

	// 		_ = login.Login(username.Text, password.Text)

	// 	},w)
	// })

	// w.SetContent(

	// 	container.NewBorder(
	// 		addBtn,
	// 		nil,
	// 		nil,
	// 		nil,

	// 	),
	// )

	w.ShowAndRun()
}
