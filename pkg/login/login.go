package login

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/cross-platform-desktop-apps/x-updater-client/internal/models"
)

func Login(name string, key string) error{

	posturl := "https://cloudflare-d1-proxy.remotejobtest8250.workers.dev/v1/consumers/login"

	body := []byte(`{
		"name": "Post title",
		"key": "Post description",
		
	}`)

	r, err := http.NewRequest("POST", posturl, bytes.NewBuffer(body))
	if err != nil {
		log.Fatal(err)

		return err
	}

	r.Header.Add("Content-Type", "application/json")

	client := &http.Client{}
	res, err := client.Do(r)
	if err != nil {

		
		return err

	}

	defer res.Body.Close()

	product := &models.Product{}
	derr := json.NewDecoder(res.Body).Decode(product)
	if derr != nil {
		return err
		
	}

	log.Println(product)


	return nil

}
